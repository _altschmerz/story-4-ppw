from django.db import models

# Create your models here.
class Activity(models.Model):
    name = models.CharField(max_length=128, default="", unique=True)

    def __str__(self):
        return self.name

class Participant(models.Model):
    name = models.CharField(max_length=128, default="")
    activity = models.ManyToManyField(Activity)

    def __str__(self):
        return self.name