from django.shortcuts import render, redirect
from .forms import *

# Create your views here.
def activities(request, act="", part=""):
    if (request.method=="POST"):
        data_part = Participant.objects.filter(name=part)
        data_act = Participant.objects.filter(activity__name=act)
        print(data_part)
        print(data_act)
        for part in data_part:
            for act in data_act:
                if part == act:
                    part.delete()

    acts = Activity.objects.all()
    data = {}
    for act in acts:
        parts = Participant.objects.filter(activity=act)
        data[act.name] = parts
    return render(request, "activities.html", {'data' : data})


def actform(request):
    if (request.method == "POST"):
        form = ActForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/Story6/activity')

    form = ActForm()
    return render(request, "actform.html", {'form' : form})


def partform(request, act):
    if (request.method == "POST"):
        form = PartForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            post.activity.add(Activity.objects.get(name=act))
            post.save()
            return redirect('/Story6/activity')

    form = PartForm()
    return render(request, "partform.html", {'form' : form})