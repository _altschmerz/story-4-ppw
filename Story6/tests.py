from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import *
from .models import *

# Create your tests here.
class TestViews(TestCase):
    def test_activities_url_exists(self):
        response = Client().get(reverse('activities'))
        self.assertEquals(response.status_code, 200)
    
    def test_activities_using_activities_template(self):
        response = Client().get(reverse('activities'))
        self.assertTemplateUsed(response, 'activities.html')

    def test_activities_using_activities_function(self):
        found = resolve('/Story6/activity')
        self.assertEqual(found.func, activities)

    def test_actform_url_exists(self):
        response = Client().get(reverse('actform'))
        self.assertEquals(response.status_code, 200)
    
    def test_actform_using_actform_template(self):
        response = Client().get(reverse('actform'))
        self.assertTemplateUsed(response, 'actform.html')

    def test_actform_using_actform_function(self):
        found = resolve('/Story6/actform/')
        self.assertEqual(found.func, actform)

    def test_partform_url_exists(self):
        pass
    #     response = Client().get(reverse('partform'))
    #     self.assertEquals(response.status_code, 200)
    
    def test_partform_using_partform_template(self):
        pass
    #     response = Client().get(reverse('partform'))
    #     self.assertTemplateUsed(response, 'partform.html')

    def test_partform_using_partform_function(self):
        pass
    #     found = resolve('/Story6/partform/')
    #     self.assertEqual(found.func, partform)


class TestModels(TestCase):
    def test_Activity_can_create_new_object(self):
        new = Activity.objects.create()
        counting_Activity_objects = Activity.objects.all().count()
        self.assertEqual(counting_Activity_objects, 1)

    def test_Participant_can_create_new_object(self):
        new = Participant.objects.create()
        counting_Participant_objects = Participant.objects.all().count()
        self.assertEqual(counting_Participant_objects, 1)
