from django.urls import path
from .views import *

urlpatterns = [
    path('activity', activities, name='activities'),
    path('activity/<str:act>/<str:part>', activities, name='activities'),
    path('actform/', actform, name='actform'),
    path('partform/<str:act>', partform, name='partform')
]