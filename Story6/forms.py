from django import forms
from .models import Activity, Participant

class ActForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = "__all__"

class PartForm(forms.ModelForm):
    class Meta:
        model = Participant
        fields = ['name']