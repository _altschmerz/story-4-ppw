from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import *
from .views import *

# Create your tests here.
class TestViews(TestCase):
    def test_track_url_exists(self):
        response = Client().get(reverse('track'))
        self.assertEquals(response.status_code, 200)
    
    def test_track_using_track_template(self):
        response = Client().get(reverse('track'))
        self.assertTemplateUsed(response, 'track.html')

    def test_track_using_track_function(self):
        found = resolve('/Story3/track')
        self.assertEqual(found.func, track)

    def test_language_url_exists(self):
        response = Client().get(reverse('language'))
        self.assertEquals(response.status_code, 200)
    
    def test_language_using_language_template(self):
        response = Client().get(reverse('language'))
        self.assertTemplateUsed(response, 'language.html')

    def test_language_using_language_function(self):
        found = resolve('/Story3/language')
        self.assertEqual(found.func, language)

    def test_carousel_url_exists(self):
        response = Client().get(reverse('carousel'))
        self.assertEquals(response.status_code, 200)
    
    def test_carousel_using_carousel_template(self):
        response = Client().get(reverse('carousel'))
        self.assertTemplateUsed(response, 'c_in_m.html')

    def test_carousel_using_carousel_function(self):
        found = resolve('/Story3/carousel')
        self.assertEqual(found.func, carousel)