from django.db import models

class Snippet(models.Model):
    course = models.CharField(max_length=255, default="", unique=True, verbose_name="Course")
    sch = models.IntegerField(default=1)
    lecturer = models.CharField(max_length=255, default="")
    desc = models.TextField(max_length=255, default="")
    term = models.CharField(max_length=255, default="")
    room = models.CharField(max_length=255, default="")

    def __str__(self):
        return self.course

class Tugas(models.Model):
    tugas = models.ForeignKey(Snippet, on_delete=models.CASCADE)
    deadline = models.DateField()