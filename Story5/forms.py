from django import forms 
from .models import Snippet

# class Schedule(forms.Form):
#     course = forms.CharField()
#     lecturer = forms.CharField()
#     desc = forms.CharField()
#     term = forms.CharField()
#     room = forms.CharField()

class SnippetForm(forms.ModelForm):
    class Meta:
        model = Snippet
        fields = '__all__'