from django.test import TestCase, Client
from django.urls import resolve, reverse
from Story5.models import * 
from Story5.views import *

# Create your tests here.
class TestViews(TestCase):
    def test_snippet_form_url_exists(self):
        response = Client().get(reverse('form'))
        self.assertEquals(response.status_code, 200)
    
    def test_snippet_using_form_template(self):
        response = Client().get(reverse('form'))
        self.assertTemplateUsed(response, 'form.html')

    def test_snippet_using_snippet_detail_function(self):
        found = resolve('/Story5/snippet/')
        self.assertEqual(found.func, snippet_detail)

    def test_schedule_url_exists(self):
        response = Client().get(reverse('schedule'))
        self.assertEquals(response.status_code, 200)
    
    def test_schedule_using_schedule_template(self):
        response = Client().get(reverse('schedule'))
        self.assertTemplateUsed(response, 'courses.html')

    def test_schedule_using_schedule_function(self):
        found = resolve('/Story5/schedule/')
        self.assertEqual(found.func, schedule)


class TestModels(TestCase):
    def test_Snippet_can_create_new_object(self):
        new = Snippet.objects.create()
        counting_Snippet_objects = Snippet.objects.all().count()
        self.assertEqual(counting_Snippet_objects, 1)