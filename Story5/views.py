from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import SnippetForm
from .models import Snippet

def snippet_detail(request):
    if (request.method == "POST"):
        form = SnippetForm(request.POST)
        if form.is_valid():
            form.save(request.POST)
            return redirect('/Story5/schedule')

    form = SnippetForm()
    return render(request, "form.html", { 'form' : form })


def schedule(request, course=""):
    if (request.method == "POST"):
        data = Snippet.objects.get(course=course)
        data.delete()

    data = Snippet.objects.all()
    return render(request, "courses.html", {'data' : data})


def details(request, course=""):
    # if (request.method == "POST"):
    data = Snippet.objects.get(course=course)
    return render(request, "details.html", {'data' : data })