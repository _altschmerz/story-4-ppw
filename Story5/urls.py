from .views import *
from django.urls import path

urlpatterns = [
    # path('', formpage, name='formpage'), 
    path('snippet/', snippet_detail, name='form'), 
    path('schedule/', schedule, name='schedule'),
    path('schedule/<str:course>', schedule, name='schedule'),
    path('details/<str:course>', details, name='details')
]